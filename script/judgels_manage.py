import csv
import json
import os
import requests
import sys
import yaml

# Read configuration file
CONFIG_DIR = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = os.path.join(CONFIG_DIR, "env.yml")
with open(CONFIG_FILE) as cfg_file:
    CONFIG = yaml.full_load(cfg_file)

JOPHIEL_BASE = "{0}://{1}".format(CONFIG["scheme"], CONFIG["jophiel_url"])
HEADERS = {'Content-type': 'application/json', 'Accept': '*/*'}
API_URL = {
    "login"    : "/api/v2/session/login",
    "add_user" : "/api/v2/users/batch-create",
    "chg_pass" : "/api/v2/user-password",
    "user_info": "/api/v2/users/{userJid}/info",
    "to_jid"   : "/api/v2/user-search/username-to-jid"
}

def get_auth_token(username, passwd):
    global HEADERS
    login_url = JOPHIEL_BASE + API_URL["login"]
    req_body = { "usernameOrEmail" : username, "password" : passwd }

    api_resp = requests.post(login_url, json.dumps(req_body), headers=HEADERS).json()
    HEADERS["Authorization"] = "Bearer " + api_resp["token"]

def add_users(users_list):
    if HEADERS.get("Authorization", None) == None:
        get_auth_token("superadmin", CONFIG["judgels_superadmin_password"])
    
    users_data = []
    for user in users_list:
        tmp = {
            "username": user[0],
            "password": user[1],
            "email": user[2] if len(user) == 3 else ""
        }
        users_data.append(tmp)
    
    add_user_url = JOPHIEL_BASE + API_URL["add_user"]
    api_resp = requests.post(add_user_url, json.dumps(users_data), headers = HEADERS).json()
    return api_resp

def change_password(username, passwd):
    if HEADERS.get("Authorization", None) == None:
        get_auth_token("superadmin", CONFIG["judgels_superadmin_password"])
    
    change_password = JOPHIEL_BASE + API_URL["chg_pass"]
    api_resp = requests.post(change_password, json.dumps({username : passwd}), headers = HEADERS).json()
    return api_resp

def username_to_jid(usernames):
    to_jid_url = JOPHIEL_BASE + API_URL["to_jid"]
    if type(usernames) != list: usernames = [usernames]

    api_resp = requests.post(to_jid_url, json.dumps(usernames), headers = HEADERS).json()
    return api_resp

def change_user_info(username, name, institutionName):
    if HEADERS.get("Authorization", None) == None:
        get_auth_token("superadmin", CONFIG["judgels_superadmin_password"])

    userJid = username_to_jid(username)[username]
    change_info_url = JOPHIEL_BASE + API_URL["user_info"].format(userJid = userJid)
    user_info = {
        "name" : name,
        "country": "ID",
        "institutionName": institutionName
    }
    api_resp = requests.put(change_info_url, json.dumps(user_info), headers = HEADERS).json()
    return api_resp

def get_user_info(username):
    if HEADERS.get("Authorization", None) == None:
        get_auth_token("superadmin", CONFIG["judgels_superadmin_password"])

    userJid = username_to_jid(username)[username]
    get_info_url = JOPHIEL_BASE + API_URL["user_info"].format(userJid = userJid)

    api_resp = requests.get(get_info_url, headers = HEADERS).json()
    return api_resp

# COMMAND
# add       : register user (ada fungsinya)
# delete    : delete user
# chgpass   : change user password (ada fungsinya)
# chginfo   : change user info (ada fungsinya)
# getinfo   : get user info (ada fungsinya)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} file_input".format(sys.argv[0]))
        exit(-1)
        
    users_acc = []
    users_info = []
    with open(sys.argv[1]) as csv_file:
        dialect = csv.Sniffer().sniff(csv_file.read(1024), delimiters="\t,")
        csv_file.seek(0)
        csv_rows = csv.reader(csv_file, dialect)
        for row in csv_rows:
            if row[0] == "No": continue
            users_acc.append([row[4], row[5], row[2]])
            users_info.append([row[1], row[3]])
    
    add_users(users_acc)
    for i in range(len(users_acc)):
        print(change_user_info(users_acc[i][0], users_info[i][0], users_info[i][1]))
