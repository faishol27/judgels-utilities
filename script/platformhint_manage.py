import csv
import json
import os
import requests
import yaml

# Read configuration file
CONFIG_DIR = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = os.path.join(CONFIG_DIR, "env.yml")
with open(CONFIG_FILE) as cfg_file:
    CONFIG = yaml.full_load(cfg_file)

PLATFORM_HINT_BASE = "{0}://{1}".format(CONFIG["scheme"], CONFIG["platform_hint_url"])
HEADERS = {'Content-type': 'application/json', 'Accept': '*/*'}
API_URL = {
    "login"     : "/login",
    "add_user"  : "/api/register-user"
}

CON = requests.Session()

def get_csrf(resp):
    ret = resp[resp.find("csrfmiddlewaretoken") + 28:]
    ret = ret[:ret.find("\">")]
    return ret

def get_auth_code(username, password):
    login_url = PLATFORM_HINT_BASE + API_URL["login"]
    resp = CON.get(login_url)
    
    data = {
        "csrfmiddlewaretoken": get_csrf(resp.text),
        "username": username,
        "password": password
    }
    CON.post(login_url, data)


def add_users(users_list):
    if CON.cookies.get("sessionid", None) == None:
        get_auth_code(CONFIG['platformhint_superadmin_user'], CONFIG['platformhint_superadmin_password'])

    users_data = []
    for user in users_list:
        tmp = {
            "username": user[0],
            "password": user[1],
            "email": user[2] if len(user) == 3 else ""
        }
        users_data.append(tmp)
    
    add_user_url = PLATFORM_HINT_BASE + API_URL["add_user"]
    api_resp = CON.post(add_user_url, json.dumps(users_data)).json()
    return api_resp

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} file_input".format(sys.argv[0]))
        exit(-1)
        
    with open(sys.argv[1]) as csv_file:
        dialect = csv.Sniffer().sniff(csv_file.read(1024), delimiters="\t,")
        csv_file.seek(0)
        csv_reader = csv.reader(csv_file, dialect)
        
        for row in csv_reader:
            if row[0] != 'No':
                user_detail = [row[-2], row[-1], row[2]]
                print(add_users([user_detail]))
