import csv
import os
import random
import sys
from xkcdpass import xkcd_password as xp

# import platformhint_manage as hint
import judgels_manage as judgels

WORDLIST = xp.generate_wordlist(min_length=4, max_length=8)

def sanitize_string(s):
    res = ""
    for c in s:
        if c.isalnum() and 32 <= ord(c) <= 128: res = res + c
        elif c == " ": res = res + " "
    return res


def generate_username(s, prefix = "", delimiter = "_", min_length = 1, max_length = 10, sanitize = True):
    if sanitize: tmp = sanitize_string(s).lower().split(" ")
    else: tmp = s.split(" ")
    
    s = []
    for e in tmp:
        if e != '': s.append(e)

    if len(s) != 0: username = s[0]
    else: username = "team"

    if len(username) < min_length and len(s) > 1:
        username += "_" + s[1]
    
    username = username[:max_length].rstrip('_')
    return prefix + delimiter + username


def generate_password(num_word, delimiter = "-"):
    passwd = xp.generate_xkcdpassword(WORDLIST, delimiter = delimiter, numwords = num_word)
    passwd = passwd + delimiter + str(random.randint(1000, 9999))
    return passwd


def main():
    file_name = os.path.basename(sys.argv[1]).replace(".csv", "")
    folder_name = os.path.dirname(sys.argv[1])
    USERNAME_PREFIX = file_name + "_"
    
    src_csv = sys.argv[1]
    out_csv = sys.argv[2] if len(sys.argv) == 3 else os.path.join(folder_name, file_name) + "-out.csv"
    
    old_cred = {}
    csv_result = []

    # Read old data
    try:
        with open(out_csv) as csv_file:
            dialect = csv.Sniffer().sniff(csv_file.read(1024), delimiters="\t,")
            csv_file.seek(0)
            csv_reader = csv.reader(csv_file, dialect)
            for row in csv_reader:
                if row[0] != 'No':
                    old_cred[row[-2]] = True
    except IOError:
        csv_result.append(['No', 'Nama Peserta', 'Email', 'Asal Sekolah', 'Username', 'Password'])
        
    with open(src_csv) as csv_file:
        dialect = csv.Sniffer().sniff(csv_file.read(1024), delimiters="\t,")
        csv_file.seek(0)
        csv_reader = csv.reader(csv_file, dialect)
        
        for row in csv_reader:
            if row[0] != 'No':
                try:
                    # Generating username and password
                    user_name = generate_username(row[1], prefix = USERNAME_PREFIX + str(row[0]), min_length = 5, max_length = 10)
                    if old_cred.get(user_name, False) == True: continue
                    
                    user_pwd = generate_password(2)
                    if len(row) > 4: user_pwd = row[-1]
                    csv_result.append(row + [user_name, user_pwd])
                
                    # Registering only new user to judgels
                    user_detail = [user_name, user_pwd, row[2]]
                    reg_api = judgels.add_users([user_detail])
                    info_api = judgels.change_user_info(user_name, row[1], row[3])
                    
                    # Registering user to hint platform
                    # hint_api = hint.add_users([user_detail])

                    print(user_name, user_pwd)
                    print("[Register]", reg_api)
                    print("[Profile]", info_api)
                    # print("[Hint]", hint_api)
                except:
                    print("[ERROR] ", user_name)
                print("")
    with open(out_csv, "a+") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerows(csv_result)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: {} file_input file_output".format(sys.argv[0]))
        exit(-1)
    
    main()
