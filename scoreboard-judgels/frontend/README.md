[logo]: https://compfest.id/static/logo_desktop-bfa453ee3b88993c9f2cba707a3b2459.svg "Compfest"
# CPC CompFest 12 Scoreboard Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0. This scoreboard is made to be working with Judgels Competitive Programming Platform.

## How to use this scoreboard

To use this scoreboard, you can change the value of the address where the endpoint of the scoreboard and contestants' data in `environments/environment.prod.ts`. You can also change the title of this scoreboard in the same file.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Author

- Samuel samuel92(at)ui.ac.id
- Faishol muhammad.faishol(at)ui.ac.id