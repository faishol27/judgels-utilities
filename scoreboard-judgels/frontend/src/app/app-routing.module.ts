import { IcpcScoreboardComponent } from './icpc-scoreboard/icpc-scoreboard.component';
// import { LandingPageComponent } from './landing-page/landing-page.component';
// import { IoiScoreboardComponent } from './ioi-scoreboard/ioi-scoreboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

const routerOptions: ExtraOptions = {
  anchorScrolling: 'enabled',
};


const routes: Routes = [
  {
    path: '',
    component: IcpcScoreboardComponent
  },
  // {
  //   path: '',
  //   component: LandingPageComponent
  // },
  // {
  //   path: '/jcpc',
  //   component: IoiScoreboardComponent
  // },
  // {
  //   path: '/scpc',
  //   component: IcpcScoreboardComponent
  // },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
