import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { LandingPageComponent } from './landing-page/landing-page.component';
// import { IoiScoreboardComponent } from './ioi-scoreboard/ioi-scoreboard.component';
import { IcpcScoreboardComponent } from './icpc-scoreboard/icpc-scoreboard.component';
import { TitleSectionComponent } from './title-section/title-section.component';
// import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    // LandingPageComponent,
    // IoiScoreboardComponent,
    IcpcScoreboardComponent,
    TitleSectionComponent,
    // NavigationBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
