import { environment } from './../../environments/environment';
import { NavigationBarService } from './../navigation-bar.service';
import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  title = 'Home | ' + environment.eventName.toUpperCase();

  constructor(public nav: NavigationBarService, private titleService: Title) { }

  ngOnInit(): void {

    this.titleService.setTitle(this.title);

    this.nav.hide();

  }

}
