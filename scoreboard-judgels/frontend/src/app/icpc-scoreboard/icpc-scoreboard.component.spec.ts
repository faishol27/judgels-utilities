import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcpcScoreboardComponent } from './icpc-scoreboard.component';

describe('IcpcScoreboardComponent', () => {
  let component: IcpcScoreboardComponent;
  let fixture: ComponentFixture<IcpcScoreboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcpcScoreboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcpcScoreboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
