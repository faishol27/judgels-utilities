## Deployments
- Pastikan sudah terinstall docker engine>=19.03.12 dan docker-compose>=1.26.2
- Jalankan `docker-compose.yml`

## Notes
### docker-compose.yml
- Ubah `RECEIVER_SECRET=RANDOM_LONG_STRING` menjadi `RECEIVER_SECRET=<STRING-RANDOM>`
- Ubah `9144:9144` menjadi `<PORT-HOST>:9144`

### Folder data
- Pastikan terdapat path `./data/contestant` dan `./data/scoreboard`

### contest-settings.json
- Lakukan restart pada docker container apabila terjadi perubahan pada file ini

## Credits
Modification from Judgels Scoreboard Receiver
